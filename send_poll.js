/**
 * Created by Loïc on 20/03/2018.
 */

const querystring = require('querystring');
const axios = require('axios');
const moment = require('moment');

let weekDayIndex = new Date().getDay();
if (weekDayIndex === 0 || weekDayIndex === 6) {
    return; //do not send script if it's the weekend
}


let propositions = {
    "Inter": "shopping_trolley",
    "Pause nat": "leaves",
    "Chez moi": "house_with_garden",
    "J'ai ma bouffe": "tent",
    "Ramadan": "man_with_turban",
    "Autre": "troll",
    "Je sais pas": "question",
};

if (weekDayIndex === 2) {
    propositions.KFC = 'poultry_leg'
}

if(moment().format('YYYY/MM/DD') === '2018/04/23'){
    propositions = {
        "Merguez Party !" : "hotdog",
        "Tant qu'y a de la braise" : "hotdog",
        "C'est pas fini !" : "hotdog",
    }
}

let truc = Object.getOwnPropertyNames(propositions).map(function(k) { return [`"${k}"`, `:${propositions[k]}:`].join(' ') }).join(' ');

var instance = axios.create({
    baseURL: 'https://some-domain.com/api/',
    timeout: 1000,
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
});

let data = {
    channel: "C13MPJXPD",
    command: '/poll',
    text: `"Qui mange où ?" ${truc}`,
    token: 'xoxp-37459302993-200530757334-337626074257-b9c11989df576dca73c23d9361dea3e9',
};
instance.post("https://slack.com/api/chat.command", querystring.stringify(data))
    .then(response => {
        console.log(response)
    });

