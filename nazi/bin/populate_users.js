const employeeRepository = require("../src/repositories/EmployeeRepository");

const moment = require("moment");

const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')


const employees = [
    {redmine_id: 22, name: 'Ely'},
    {redmine_id: 41, name: 'Enguerran'},
    {redmine_id: 42, name: 'Max'},
    {redmine_id: 26, name: 'Simon'},
    {redmine_id: 4, name: 'Vincent'},
    {redmine_id: 25, name: 'Xavier'},
    {redmine_id: 146, name: 'Jules'},
    {redmine_id: 152, name: 'Loic'},
    {redmine_id: 31, name: 'Manitra'},
    {redmine_id: 164, name: 'Alexandre'},
    {redmine_id: 165, name: 'Baptiste'}
];

employeeRepository.saveAll(employees);