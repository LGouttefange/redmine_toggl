const moment = require("moment");

const employeeRepository =  require('../src/repositories/EmployeeRepository');
const eeportRepository =  require('../src/repositories/ReportRepository');

const redmine_api = require('../src/infrastructure_services/Redmine');
const yesterday = moment().subtract(1,'day').format('YYYY-MM-DD');

function populate(time_entries) {
    let report = employeeRepository.all().map(employee => {
        return {redmine_id: employee.redmine_id, time_spent: 0}
    });
    time_entries.map((time_entry)=>{
        let employee = null;
        try{
            employee =  report.find({redmine_id: time_entry.user.id});
            employee.time_spent += time_entry.hours;
        }catch(e){
            return;
        }

    });
    report = report.filter(employee=> employee.time_spent < 7);

    let entry = {};
    entry[yesterday] = report;
    db.push(entry)
}

redmine_api.getTimeEntries({
    spent_on: yesterday,
    limit: 100
}).success(function(response){
    try {
        populate(response.time_entries);
    } catch (e) {
        console.log(e);
    }
})
    .error((e)=> {
        throw e;
    });
