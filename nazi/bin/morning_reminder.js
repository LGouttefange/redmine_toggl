const moment = require("moment");

const Redmine = require('promised-redmine');
var config = {
    host: "redmine.dotsafe.fr", // required
    apiKey: "fd09fa226345d183fd3d62cc651b719462e3e54a", // required
    pathPrefix: "/redmine",
    protocol: "https"
};
const redmine_api = new Redmine(config);

function populate(time_entries) {
    const employees = {};
    time_entries.map((time_entry)=>{
        employees[time_entry.user.id] = employees[time_entry.user.id] || 0;
        employees[time_entry.user.id] += time_entry.hours;
    });
    db.push(employees)
}

redmine_api.getTimeEntries({
    spent_on: moment().subtract(1,'day').format('YYYY-MM-DD')
}).success(function(response){
    populate(response.time_entries);
})
    .error((e)=> {
        throw e;
    });
