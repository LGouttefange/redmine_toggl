
const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const db = lowdb(new FileSync('../../var/db/reports.json'));

class ReportRepository{
    constructor(){
        db.defaults({ reports: [] }).write();
    }

    all(){
        return db.get('reports').value();
    }

    saveAll(employees){
        return db.get('reports').push(employees).write();
    }
};

module.exports = new ReportRepository();