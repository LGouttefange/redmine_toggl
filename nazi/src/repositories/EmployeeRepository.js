
const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const db = lowdb(new FileSync('../../var/db/employees.json'));

class EmployeeRepository{
    constructor(){
        db.defaults({ employees: [] }).write();
    }

    all(){
        return db.get('employees').value();
    }

    findOrFail(id){
        let employee = this.find(id);
        if(!employee){
            throw new Error('Employee not found');
        }
        return employee;
    }

    find(id) {
        return db.get('employees').find({redmine_id: id}).value();
    }

    saveAll(employees){
        return db.set('employees', employees).write();
    }
};

module.exports = new EmployeeRepository();