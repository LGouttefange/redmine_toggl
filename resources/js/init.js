const REDMINE_STORAGE_KEY = 'redmine_key';
const TOGGL_STORAGE_KEY = 'toggl_key';
const DATE_FORMAT = 'YYYY-MM-DD';
import VTooltip from 'v-tooltip'
import Datepicker from './components/datepicker.vue'
import VueScrollTo from 'vue-scrollto'
import Notifications from 'vue-notification'
import browserify_window from "browserify-window";

let Vue = require('vue/dist/vue.min.js');
const MobileDetect = require('mobile-detect');
require('moment/locale/fr');

window.md = new MobileDetect(window.navigator.userAgent);
window.axios = require('axios');
window.moment = require('moment');
global._ = window._  = require('lodash');

Vue.use(Notifications);
Vue.use(VTooltip);
Vue.use(Datepicker);
Vue.use(VueScrollTo);

moment.locale('fr');
axios.interceptors.request.use(function (config) {
    return config;
});

axios.interceptors.response.use(function (config) {
    // loader.status = 'done';
    // setTimeout(()=>{
    //     loader.status = 'pending'
    // }, 1000);
    return config;
}, function (error) {
    loader.status = 'done';
    account.show = true;
});


let account = new Vue({
    el: "#account",
    data: {
        show: false,
        keys: {
            toggl: localStorage.getItem(TOGGL_STORAGE_KEY),
            redmine: localStorage.getItem(REDMINE_STORAGE_KEY),
        }
    },
    watch: {
        keys: {
            deep: true,
            handler: function (new_value, old_value) {
                localStorage.setItem(TOGGL_STORAGE_KEY, new_value.toggl);
                localStorage.setItem(REDMINE_STORAGE_KEY, new_value.redmine);
            }
        }
    },
    methods:{
        hideAccount: function(){
            account.show = false
        }
    }
});


let top_menu = new Vue({
    el: "#top-menu",
    methods:{
        showAccount: function(){
            account.show = true
        }
    }
});


let period_form = new Vue({
    el: '#period_form',
    components: {Datepicker},
    data: {
        beginning_date: moment().format(DATE_FORMAT),
        ending_date: moment().format(DATE_FORMAT),
    },
    methods: {
        refreshActivitiesWithDates: function (dates) {
            this.beginning_date = moment(dates.start).format(DATE_FORMAT);
            this.ending_date = moment(dates.end).format(DATE_FORMAT);
            this.refreshActivities();
        },
        refreshActivities: function (a, b, c) {
            loader.status = 'loading';
            axios.get('/activities', {
                params: {
                    beginning_date: this.beginning_date,
                    ending_date: this.ending_date,
                    toggl_api_key: account.keys.toggl
                }
            })
            .then(function (results) {
                loader.status = 'done';
                report.report = results.data;
                Object.keys(report.report).length === 0 ? report.show = false : report.show = true;
            });
            this.$scrollTo('#content', 500);
        },
        fetchActivities: function (e) {
            if (e !== undefined)
                e.preventDefault();
            this.refreshActivities();
        }
    },
    watch: {
    }
});


let report = new Vue({
    el: "#report",
    data: {
        show: false,
        report: {}
    },
    filters: {
        rounded: function (number) {
            return Math.round((number) / 0.25) * 0.25;
        }
    },
    methods: {

        dayTime(day){
            var report = this;
            return Object.entries(day.issues).map(issue => {
                return report.issueTime(issue[1])
            }).reduce((a, b) => a * 1 + b * 1, 0)
        },
        rounded: function (number) {
            return Math.round((number) / 0.25) * 0.25;
        },

        issueTime: function (issue) {
            return Object.entries(issue.activities).map(activity => {
                return activity[1].time_spent
            }).reduce((a, b) => a * 1 + b * 1, 0)
        },

        sendReport: function (e) {
            e.preventDefault();
            axios.post("/send_report", {
                report: this.report,
                redmine_api_key: account.keys.redmine
            })
                .then(() =>{
                    this.report = {};
                    this.$notify({
                        group: "all",
                        title: ' ',
                        text: 'Mamene! Ton temps a bien été saisi sur Redmine ~(˘▾˘~)',
                        type: "toast",
                        position: ['left', 'bottom']
                    });
                });
        },

        dayIsValid: function (day) {
            return (this.dayTime(day) >= 7);
        },
        dayValidityClass(day){
            return this.dayIsValid(day) ? 'is-valid' : 'is-wrong';
        },

        reportIsInvalid(){
            let v_report = this;
            let days = Object.values(this.report);
            let report_validity = days.map(day => {
                return v_report.dayIsValid(day)
            }).reduce((a, b) => {
                return a && b;
            }, true);
            return report_validity === false;
        },

        showIssueDetails(issue, a, b){
            let Vreport = this;
            let event = a;
            axios.get("/issues/" + issue.id + "/description", {
                params: {
                    "redmine_api_key": account.keys.redmine
                }
            })
                .then(function (response) {
                    // this.showIssueToolTip(issue, response.desc)
                    Vue.set(issue, 'description', response.data)
                    event.target.dispatchEvent(new Event('mouseover'));
                });

        }
    }
});

Vue.filter('uppercase', function (value) {
    return value.toUpperCase()
})
Vue.filter('rounded', function (value) {
    return Math.round((value) / 0.25) * 0.25;
})

Vue.filter('formatted', function (value) {
    let moment_date = moment(value);
    return _.startCase(_.camelCase(moment_date.format('dddd'))) +" "+ moment_date.format('D/MM');
})


var loader = new Vue({
    el: "#loader",
    data: {
        status: 'pending',
    },
    methods:{
        getClass: function(){
            if (this.status === 'pending')
                return "pending";
            if (this.status === 'loading')
                return "is-active";
            if (this.status === 'done')
                return "done";
        }
    }
})

period_form.fetchActivities();
loader.status = 'loading';