module.exports = class Activity {

    constructor(id) {
        this.id = id;
        this.time_spent = 0;
    }

    getTimeSpent(){
        return this.time_spent;
    }

};