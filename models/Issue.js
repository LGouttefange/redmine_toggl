const Activity = require('./Activity');
module.exports = class Issue {

    constructor(id) {
        this.id = id;
        this.activities = {};
        this.description = "<span class='loader'></span>"; //du sale mamene
    }


    findActivityOrNew(activity_id) {
        if(!this.activities.hasOwnProperty(activity_id)){
            this.activities[activity_id] =  new Activity(activity_id);
        }
        return this.activities[activity_id];
    }

    getTimeSpent(){
        let time_spent = 0;
        Object.entries(this.activities).forEach(function(issue){
            time_spent += issue.time_spent;
        });
        return time_spent;
    }
};