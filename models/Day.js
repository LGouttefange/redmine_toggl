
const Issue = require('./Issue');

module.exports = class Day {

    constructor(date) {
        this.date = date;
        this.issues = {};
    }

    findIssueOrNew(issue_id) {
        if(!this.issues.hasOwnProperty(issue_id)){
            this.issues[issue_id] = new Issue(issue_id);
        }
        return this.issues[issue_id];
    }

    getTimeSpent(){
        let time_spent = 0;
        Object.entries(this.issues).forEach(function(issue){
            time_spent += issue.time_spent;
        });
        return time_spent;
    }
};