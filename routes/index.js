var express = require('express');
var router = express.Router();
const argv = require('yargs').argv;
const fs = require('fs');
const csv = require('fast-csv');
let process = require('process');
const chalk = require('chalk');
const axios = require('axios');
const TogglClient = require('toggl-api');
const ValidationFailedHandler = require("../src/handlers/ValidationFailedhandler");
let validationFailedHandler = new ValidationFailedHandler();
let timeReporter = new (require('../src/utils/TimeCheckerReporter'))();
const async = require('async');

const Redmine = require('node-redmine');
let redmine_api;


const Parsers = {
    Hour: require('../src/parsers/hour').default,
    Activity: require('../src/parsers/activity').default,
    Date: require('../src/parsers/date').default
};

Object.entries = function (obj) {
    var ownProps = Object.keys(obj),
        i = ownProps.length,
        resArray = new Array(i); // preallocate the Array
    while (i--)
        resArray[i] = obj[ownProps[i]];

    return resArray;
};

Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {});
});

let treatWorkspace = function (toggl, beginning_date, ending_date,workspace, async) {
    toggl.detailedReport({
        "workspace_id": workspace.id,
        "since": beginning_date,
        "until": ending_date,
        "user_agent": "redmine"
    }, function (a, data, c) {
        let time_entries = data.data;
        Object.entries(time_entries).forEach(function (time_entry) {
            if (!/#(\d+)/g.exec(time_entry.description)) {
                return;
            }
            let issue_id = (/#(\d+)/g.exec(time_entry.description)[1]);
            let time_spent = Parsers.Hour.handle(time_entry.dur);
            date_time_was_spent_on = time_entry.start.substr(0, 10);

            let time_entry_data = {
                "issue_id": issue_id,
                'hours': Parsers.Hour.round(time_spent),
                'activity_id': Parsers.Activity.parse(time_entry.tags),
                'date': date_time_was_spent_on
            };
            timeReporter.stock(time_entry_data)
        });
        async();
    });
};
router.get('/activities', function (req, res, next) {
    let beginning_date = req.query.beginning_date;
    let ending_date = req.query.ending_date;

    timeReporter.setDryRun(true);
    let toggl = new TogglClient({apiToken: req.query.toggl_api_key});

    try {
        timeReporter.clearReport();
        toggl.getWorkspaces(function (error, workspaces) {
            if(error !== null && error !== undefined){
                res.send(error);
            }
            async.each(workspaces, treatWorkspace.bind(this,toggl,beginning_date,ending_date), function(){
                timeReporter.sort_report();
                res.send(JSON.stringify(timeReporter.report));
            });
        })
    } catch (e) {
        res.send(400);
    }

})
;

router.post('/send_report', function (req, res) {

    redmine_api = new Redmine("https://redmine.dotsafe.fr/redmine", {apiKey: req.body.redmine_api_key})

    Object.entries(req.body.report).forEach(function (day) {
        Object.entries(day.issues).forEach(function (issue) {
            Object.entries(issue.activities).forEach(function (activity) {
                let hours = Parsers.Hour.round(activity.time_spent);
                if(hours == 0){
                    return;
                }
                let time_entry = {
                    "issue_id": issue.id,
                    'hours': hours,
                    'activity_id': Parsers.Activity.handle(activity.id),
                    'spent_on': day.date

                };
                redmine_api.create_time_entry({time_entry: time_entry}, function (error) {
                    if (error) {
                        console.error(JSON.stringify(time_entry) + " - ERREUR : " + JSON.stringify(error))
                    }
                })
            })
        })
    })
    res.send(200);
});

module.exports = router;
