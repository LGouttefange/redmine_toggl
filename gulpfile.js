'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-uglify');
var sass = require('gulp-sass');
const babel = require('gulp-babel');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var vueify = require('vueify');
const debug = require('gulp-debug');
const gutil = require('gulp-util');
const livereload = require('gulp-livereload');
const plumber = require('gulp-plumber');

gulp.task('clean', function (cb) {
    require('rimraf')('dist', cb);
});




gulp.task('js', function () {
    browserify({entries: 'resources/js/init.js', extensions: ['.js'], debug: true})
        .transform(vueify)
        .bundle()
        .on('error', function(err){
            console.log(err.stack);
            this.emit('end');
        })
        // .pipe(livereload())
        .pipe(source('app.js'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('css', function () {

    var libs = [
        'node_modules/v-calendar/lib/v-calendar.min.css',
    ];
    return gulp
        .src(libs)
        .pipe(plumber())
        // .pipe(livereload())
        .pipe(concat("lib.css"))
        .pipe(plumber())
        .pipe(gulp.dest('public/css'))
})

gulp.task('sass', ['css'], function () {
    var sass = require('gulp-sass');

    return gulp
        .src('resources/sass/*.sass')
        // .pipe(livereload())
        .pipe(sass({
            precision: 10,
            includePaths: [
                'node_modules/bulma',
                'node_modules',
            ]
        }))
        .pipe(plumber())
        .pipe(gulp.dest('public/css'));
});

// gulp.task('images', function () {
//     var cache = require('gulp-cache'),
//         imagemin = require('gulp-imagemin');
//
//     return gulp.src('app/images/**/*')
//         .pipe(cache(imagemin({
//             progressive: true,
//             interlaced: true
//         })))
//         .pipe(gulp.dest('dist/images'));
// });

gulp.task('fonts', function () {
    return gulp.src('resources/fonts/*')
        .pipe(gulp.dest('public/fonts'));
});


gulp.task('html', ['sass'], function () {
    var uglify = require('gulp-uglify'),
        minifyCss = require('gulp-minify-css'),
        useref = require('gulp-useref'),
        gulpif = require("gulp-if"),
        assets = useref.assets();

    return gulp.src('app/*.html')
        .pipe(assets)
        .pipe(plumber(function (error) {
            gutil.log(error.message);
            this.emit('end');
        }))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('dist'));
});


gulp.task('watch', function() {
    gulp.watch('resources/sass/**/*.sass', ['sass']);
    gulp.watch(['resources/js/**/*.js', 'resources/js/**/*.vue'], ['js']);
    livereload.listen();
});
gulp.task('serve', ['build', 'watch'], function () {
});

gulp.task('build', ['sass', 'js']);

gulp.task('default', function () {
    gulp.start('build');
});