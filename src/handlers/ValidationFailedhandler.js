const fs = require("fs");

class ValidationFailedhandler {
    constructor() {
        this._path_to_error_file = '_errors.csv';
    }

    handle(data, message){
        this.init(data);
        fs.appendFile(this._path_to_error_file, message + "," + Object.keys(data).map(key => data[key]).join(',') + "\r");
    }


    init(data){
        if(this.in_process){
            return;
        }
        fs.writeFile(this._path_to_error_file, "Message d'erreur,"+Object.keys(data).join(',') + "\n");
        this.in_process = true;
    }

    errorsWereFound(){
        return this.in_process;
    }
}

module.exports = ValidationFailedhandler;