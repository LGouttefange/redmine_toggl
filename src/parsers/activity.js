const ACTIVITY_BY_KEY = {
    'dev': 9,
    'ges': 11,
    'com':17,
    'con':8,
    'int':12,
    'rec':13,
    'cor':14,
    'mis':15,
    'mep':15,
    'inf':10,
    'fac':16,
    'ass':18,
}

exports.default = {



    toTag(tags) {
        return tags.toLowerCase().substr(0, 3);
    },
    handle(tags){

        let tagName = this.toTag(tags);
        return ACTIVITY_BY_KEY[tagName];
    },

    parse(tags){
        return this.toTag(tags.length === 0 ? 'developpement' : tags[0]);
    }
};