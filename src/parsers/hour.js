exports.default = {
    handle(duration)
    {
        return (duration*1.0 / (3600*1000));
        //15 => 0.25, 60 => 1
    },

    round(duration){
        return Math.round((duration) / 0.25) * 0.25;
    },
    roundToDecimal(duration){
        return duration.toFixed(2);
    }
};