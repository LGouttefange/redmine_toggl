
class AbstractValidator  {


    constructor(validationFailedHandler) {
        this._validationFailedHandler = validationFailedHandler;
    }

    handle(data) {
        if (this.failsValidation(data)) {
            this._validationFailedHandler.handle(data, this.getMessage())
            return false;
        }
        return true;
    }



    failsValidation(data) {
        return !this.passesValidation(data);
    }
}

module.exports = AbstractValidator;
module.exports.new = function(truc){
    return new module.exports(truc);
};