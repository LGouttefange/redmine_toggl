AbstractValidator = require("./AbstractValidator");

class HasRedmineIssueInDescription extends AbstractValidator {
    getMessage () {
        return "Issue could not be found in description"
    }


    passesValidation(data){
        return /#\d+/.test(data['Description'])
    }

};

module.exports = HasRedmineIssueInDescription;
module.exports.new = function(truc){
    return new module.exports(truc);
};