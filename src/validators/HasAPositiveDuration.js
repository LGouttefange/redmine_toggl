AbstractValidator = require('./AbstractValidator');

class HasAPositiveDuration extends AbstractValidator {


    getMessage() {
        return "Issue could not be found in description"
    }

    passesValidation(data) {
        return true
    }
}

module.exports = HasAPositiveDuration;
module.exports.new = function(truc){
    return new module.exports(truc);
};
