const chalk = require('chalk');
const HourParser = require('../parsers/hour').default;

const Day = require('../../models/Day');
const Activity = require('../../models/Activity');
const Issue = require('../../models/Issue');

class TimeCheckerReporter {

    constructor() {
        this.report = {};
        this.is_dry_run = true;
    }

    setDryRun(is_dry_run) {
        this.is_dry_run = is_dry_run;
    }

    execute() {
        this.sort_report();
        for (let date in this.report) {
            let time_spent_this_day = 0;
            for (let issue in this.report[date]) {
                for (let activity in this.report[date][issue]) {
                    let time_spent = this.report[date][issue][activity];
                    let rounded_time_spent = HourParser.round(time_spent);
                    time_spent_this_day += rounded_time_spent;
                    this.log_issue({
                        "issue_id": issue,
                        'hours': rounded_time_spent,
                        'activity_id': activity,
                        'spent_on': date
                    })
                }

            }
            console.log(chalk.bold.blue("TOTAL DE LA JOURNEE : " + time_spent_this_day));
            this.showWarnings(date, time_spent_this_day);
        }
    }

    sort_report() {
        let sorted_report = {};
        let report = this.report;
        Object.keys(this.report).sort().forEach(function (key) {
            sorted_report[key] = report[key];
        });
        this.report = sorted_report;
    }

    log_issue(time_entry) {
        if (time_entry.hours == 0)
            return;
        //on appelle redmine
        let log_message = time_entry.hours + " heures passées le " + time_entry.spent_on + " sur la tâche n°" + time_entry.issue_id;
        if (!this.is_dry_run) {
            this.redmine_api.create_time_entry({time_entry: time_entry}, function (a) {
                if (a !== null)
                    console.log(a)
            });
        }
        else {
            console.log(chalk.green("SIMULATION :: " + log_message));
        }
    }

    showWarnings(date, hours) {
        if (hours > 7) {
            console.log("Plus de sept heures enregistrées le " + date + "(" + hours + " heures)");
        }
        if (hours < 7) {
            console.log(chalk.red("Il manque des heures le " + date + chalk.bold.red("(" + (7 - hours) + " heures)")));
        }
    }

    stock(time_entry) {

        let date = this.findDateOrNew(time_entry.date);
        let issue = date.findIssueOrNew(time_entry.issue_id);
        let activity = issue.findActivityOrNew(time_entry.activity_id);

        activity.time_spent += parseFloat( time_entry.hours);
    }

    setRedmineApi(redmine_api) {
        this.redmine_api = redmine_api;
    }

    findDateOrNew(date) {
        if(!this.report.hasOwnProperty(date)){
            this.report[date] = new Day(date);
        }
        return this.report[date];
    }

    clearReport() {
        this.report = {};
    }

}

module.exports = TimeCheckerReporter;